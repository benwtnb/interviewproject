import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  Navigator
} from 'react-native';

var CardView = require('./cardView');
var PreviewCard = require('./previewCard');
var FullView = require('./fullView');
//var fetchMetadata = require('./fetchMetadata');

let messageText = [
  'Just a plain string',
  'These 10 tips are great! https://medium.com/i-love-charts/if-justin-bieber-were-my-boyfriend-id-tell-him-to-read-this-1cfd85357353#.lt22719oj',
  'I love this song https://www.youtube.com/watch?v=kffacxfA7G4',
  'True story https://twitter.com/stephaniedoranx/status/793383655695642624',
  'Flash back to the 80\'s https://soundcloud.com/jerry-shen-403158940/what-do-you-mean-its-1985-justin-bieber',
  'For real though http://giphy.com/gifs/justin-bieber-i0PCCZyb96uiI',
  'https://www.producthunt.com/posts/bieber-bomb is great'
]

let urlPattern = /(http|https):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:\/~+#-]*[\w@?^=%&\/~+#-])?/;
let faviconPattern = /<link.*?rel(\S)*("|\').*icon("|\')(.*?)("|\')>/gi;
let imagePattern = /<img\s[^>]*?src\s*=\s*['\"]([^'\"]*?)['\"][^>]*?>/i;
let titlePattern = /<title[^>]*>([^<]+)<\/title>/;
//old pattern /((http[s]?):\/)?\/?([^:\/\s]+)((\/\w+)*\/)([\w\-\.]+[^#?\s]+)(.*)?(#[\w\-]+)?$/

let ignoredSites = ['youtube','giphy','soundcloud','twitter'];

class HomeView extends Component {
  constructor(props) {
    super(props);
    fixedState = {mainData:{},refresh:false,preview:null,fullText:null};
    for (var k in messageText) {
      fixedState.mainData[messageText[k]] = {url:'',title:'',image:'',name:''};
    }
    this.state = fixedState;
  }

  componentWillMount() {
    var _state = this;
    var newState = {};

    function fetchMetadata(url,message,siteName) {
      try {
        return fetch(url)
        .then((responseData) => {
          var body = responseData._bodyInit;
          var title = body.match(titlePattern)[1];
          var image = body.match(imagePattern)[1];
          var responseJson = JSON.stringify({url:url,title:title,image:image,name:siteName});
          newState = _state.state.mainData;
          newState[message] = responseJson;
          _state.setState({mainData: newState});
          return responseJson
        })
        .catch((error) => {
          console.error(error);
        });
      } catch(error) {
        console.error(error);
      }
    }

    for (var k in this.state.mainData) {
      var matchedUrl = urlPattern.exec(k);
      if (matchedUrl != null) {

        var cleanUrl = matchedUrl.toString().trim().split(/[\s,]+/)[0]; //clean trailing whitespaces picked up

        //create a clean site name
        var siteName = cleanUrl.split("/")[2];
        if(siteName.split(".")[0]=='www') {
          var siteNameArr = siteName.split(".");
          var newSiteName = "";
          for (var part =1; part< siteNameArr.length ;part +=1) {
            newSiteName +=  siteNameArr[part];
            if(part>=1 && part < siteNameArr.length-1) {
              newSiteName += ".";
            }
          }
          siteName = newSiteName;
        }

        var blocked = false;
        console.log(siteName);
        console.log("MATCHES");
        for (var index in ignoredSites)
        {
          console.log(ignoredSites[index]);
          if (siteName.search(ignoredSites[index]) != -1) {
            //url not in the list of ignored sites
            console.log('FOUND');
            blocked = true;
            break;
          }
        }

        if (blocked == false) {
          fetchMetadata(cleanUrl,k,siteName);
        }
      }
    }
  }


  render() {
    var _state = this;
    var counter = -1; //for generating key id
    var messageData = [];
    for (var k in this.state.mainData) {
      messageData.push({'title':k,'metaData':this.state.mainData[k]});
    }

    //----------------------------Preview handling functions------------------
    function openPreview(title) {
      //handle previews
      var index = messageText.indexOf(title);
      _state.setState({preview: index,fullText:title});
    }

    function closePreview() {
      //close open previews
      _state.setState({preview: null,fullText:null});
    }
    //--------------------------------------------------------------------------------

    //------------------------For navigating to full screen webview--------------------
    function navFullScreen(metaData,fullText){
      _state.props.navigator.push({
        id: 'fullView',
        metaData: metaData,
        fullText:fullText
      })

    }
    //----------------------------------------------------------------------------------

    return (<ScrollView style={styles.scrollMain}>
      <View>
      {messageData.map(function(message,index){
        //counter += 1;
        if(message.metaData.url == '') {
          return <Text key={index} style={styles.tweetText}>{message.title}</Text>;
        }
        else {
          if(_state.state.preview == index) {
            return <View key={index}>
            <Text style={styles.tweetText}>{message.title}</Text>
            <View style={styles.card}>
            <View style={styles.previewOptions}>
            <TouchableOpacity onPress={() => navFullScreen(message.metaData,message.title)} style={styles.fullScreenButton}><Text style={styles.openText}>FULL SCREEN</Text></TouchableOpacity>
            <TouchableOpacity onPress={() => closePreview()} style={styles.closeButton}><Text style={styles.closeText}>CLOSE</Text></TouchableOpacity>
            </View>
            <PreviewCard key={index} metaData={message.metaData} />
            </View>
            </View>;
          }
          else {
            return <View key={index}><Text style={styles.tweetText}>{message.title}</Text><TouchableOpacity onPress={() => openPreview(message.title)}><CardView  key={index} metaData={message.metaData} /></TouchableOpacity></View>;
          }
        }
      })}
      </View>
      </ScrollView>
    );
  }
}

let avatarSize = 24;
let marginSize = 6;
let gutterSize = 8;
let textInset = gutterSize + (marginSize * 2);
let borderColor = '#999999';
let childBorder = '#CCCCCC';
let subTextColor = '#999999';
let tappableColor = '#25BCCA';
let marginSpace = 5;
let backgroundColor = '#f6406c';
let titleColor = '#ffffff';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const styles = StyleSheet.create({
  scrollMain: {
    height: height,
    paddingTop: 20,
    paddingBottom: 44,
  },
  tweetText: {
    paddingTop: marginSize * 2,
    paddingLeft: textInset,
    paddingBottom: marginSize * 2,
    paddingRight: textInset,
    fontSize: 22,
    lineHeight: 32,
    fontWeight: '300',
  },
  card: {
    paddingTop: marginSize * 2,
    paddingBottom: marginSize * 2,
    backgroundColor: backgroundColor,
    margin: marginSpace,
    borderRadius: 5,
    height: width + 40
  },
  previewOptions: {
    flexDirection: 'row'
  },
  closeText: {
    color: titleColor,
  },
  closeButton: {
    marginRight: textInset,
    marginLeft: textInset,
    flex:1,
    alignItems:'flex-end'
  },
  openText: {
    color: titleColor,
  },
  fullScreenButton: {
    flex:0,
    alignItems:'flex-start',
    marginRight: textInset,
    marginLeft: textInset,
  }
});

module.exports = HomeView;
