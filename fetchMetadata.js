function fetchMetadata(url) {

    try {
    return fetch(url)
    .then((response) => response)
    .then((responseData) => {
        var body = responseData._bodyInit;
        var title = body.match(/<title[^>]*>([^<]+)<\/title>/)[1];
        var responseJson = JSON.stringify({url:url,title:title});
        console.log(responseJson);
        return responseJson
      })
      .catch((error) => {
        console.error(error);
      });
    } catch(error) {
      console.error(error);
    }
  }
module.exports = fetchMetadata;
