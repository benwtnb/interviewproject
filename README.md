# README #

The goal of this interview project is to create a very simplified version of how Medium uses Embedly and previewing a website in an inline preview The flow of how it operates would be:

1. Detect if a string/tweet contains a url.
2. If it does have a URL display a card below the string with a preview of the metadata for that site, showing the site/story's: Image, Title, a cleaned up version of the Sitename
   (URL's from YouTUbe, Twitter, Soundcloud, and Giphy should ignore this behavior.)
3. If the user taps on the card it expands in line showing the site in a square preview window that can be scrolled.
4. Around the preview there should be buttons to close the frame or open it in a dedicated webview.
5. Bonus👍 If the user chooses to open it in a webview, the original string is still displayed at the top of webview.