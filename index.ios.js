/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  Navigator
} from 'react-native';

var CardView = require('./cardView');
var PreviewCard = require('./previewCard');
var FullView = require('./fullView');
var HomeView = require('./homeView');
//var fetchMetadata = require('./fetchMetadata');

export default class interviewProject extends Component {

  render() {
    return (
      <Navigator
        initialRoute={{id: 'home'}}
        renderScene={this.navigatorRenderScene}/>
    );
  }

  navigatorRenderScene(route, navigator) {
    _navigator = navigator;
    switch (route.id) {
      case 'home':
      return (<HomeView navigator={navigator} title="Home"/>);
      case 'fullView':
      return (<FullView navigator={navigator} title="Full View" metaData={route.metaData} fullText={route.fullText}/>);
    }
  }

}

AppRegistry.registerComponent('interviewProject', () => interviewProject);
