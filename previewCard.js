var React = require('react');
var ReactNative = require('react-native');
var {
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableNativeFeedback,
  View,
  Dimensions,
  WebView
} = ReactNative;

class PreviewCard extends React.Component {
  render() {
    var TouchableElement = TouchableHighlight;
    if (Platform.OS === 'android') {
      TouchableElement = TouchableNativeFeedback;
    }
    
    var metaData = JSON.parse(this.props.metaData);
    var url = metaData.url;
    var title = metaData.title;
    return (
      <WebView  source={{uri: url}} style={styles.previewArea}/>
    );

  }
}

let avatarSize = 24;
let marginSize = 6;
let marginSpace = 5;
let gutterSize = 8;
let textInset = gutterSize + (marginSize * 2);
let borderColor = '#999999';
let childBorder = '#CCCCCC';
let subTextColor = '#999999';
let tappableColor = '#25BCCA';
let backgroundColor = '#f6406c';
let titleColor = '#ffffff';
let nameColor = '#ffffff';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const styles = StyleSheet.create({
  textContainer: {
    flex: 1,
  },
  card: {
    paddingTop: marginSize * 2,
    paddingBottom: marginSize * 2,
    backgroundColor: backgroundColor,
    margin: marginSpace,
    borderRadius: 5,
    height: width + 40
  },
  previewArea: {
    marginTop: marginSpace,
    marginBottom: marginSpace,
    height:width - 20
  },
  previewOptions: {
    flexDirection: 'row'
  },
  closeText: {
    flex:1,
    color: titleColor,
    marginRight: 0,
    marginLeft: textInset,
    alignItems: 'flex-end'
  },
  openText: {
    flex:1,
    color: titleColor,
    marginRight: textInset,
    marginLeft: textInset,
    alignItems: 'flex-end'
  },
});

module.exports = PreviewCard;
