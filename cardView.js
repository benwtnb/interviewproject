var React = require('react');
var ReactNative = require('react-native');
var {
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableNativeFeedback,
  View,
  Dimensions
} = ReactNative;

class CardView extends React.Component {
  render() {
    var TouchableElement = TouchableHighlight;
    if (Platform.OS === 'android') {
      TouchableElement = TouchableNativeFeedback;
    }

    var metaData = JSON.parse(this.props.metaData);
    var title = metaData.title;
    var url = metaData.url;
    var name = metaData.name;
    var image = metaData.image;

    //------------------sanitize relative image urls-------------
    if(image.split("/")[0]=='') {
      //image may have relative path
      image = url+"/"+image;
    }
    //-----------------------------------------------------------

    if (image == ''){
      //will not display image
      return (
        <View style={styles.card}>
          <Text style={styles.nameText}>{name}</Text>
          <Text style={styles.titleText}>{title}</Text>
        </View>
      );
    }
    else {
      return (
        <View style={styles.card}>
          <Image source={{uri: image}} style={styles.imageArea} />
          <Text style={styles.nameText}>{name}</Text>
          <Text style={styles.titleText}>{title}</Text>
        </View>
      );
    }
  }
}

let avatarSize = 24;
let marginSize = 6;
let marginSpace = 10;
let gutterSize = 8;
let textInset = gutterSize + (marginSize * 2);
let borderColor = '#999999';
let childBorder = '#CCCCCC';
let subTextColor = '#999999';
let tappableColor = '#25BCCA';
let backgroundColor = '#f6406c';
let titleColor = '#ffffff';
let nameColor = '#ffffff';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const styles = StyleSheet.create({
  textContainer: {
    flex: 1,
  },
  card: {
    paddingTop: marginSize * 2,
    paddingLeft: textInset,
    paddingBottom: marginSize * 2,
    paddingRight: textInset,
    backgroundColor: backgroundColor,
    margin: marginSpace,
    borderRadius: 5,
  },
  nameText: {
    color: nameColor,
    fontWeight: "bold"
  },
  titleText: {
    color: titleColor
  },
  imageArea: {
    height:200,
    resizeMode: 'cover'
  }
});

module.exports = CardView;
