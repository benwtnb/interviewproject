var React = require('react');
var ReactNative = require('react-native');
var {
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableNativeFeedback,
  View,
  Dimensions,
  Navigator,
  WebView,
  TouchableOpacity
} = ReactNative;
var HomeView = require('./homeView');

class FullView extends React.Component {

  render() {
    var _state = this;
    var TouchableElement = TouchableHighlight;
    if (Platform.OS === 'android') {
      TouchableElement = TouchableNativeFeedback;
    }

    function goBack() {
      _state.props.navigator.pop({
        id: 'fullView'
      })
    }

    var metaData = JSON.parse(this.props.metaData);
    var fullText = this.props.fullText;
    if (fullText.length > 90) {
      fullText = fullText.substring(0, 85)+"...";
    }
    var url = metaData.url;
    var back = "<";
    return (
      <View style={styles.fullScreenContainer}>
      <View style={styles.navigationBar}>
      <TouchableOpacity onPress={()=>goBack()}>
      <Text style={styles.backText} >{back}</Text>
      </TouchableOpacity>
      <Text style={styles.fullTextArea}>{fullText}</Text>
      </View>
      <WebView style={styles.webView} source={{uri: url}}/>
      </View>
    );
  }
}

let avatarSize = 24;
let marginSize = 6;
let marginSpace = 10;
let gutterSize = 8;
let textInset = gutterSize + (marginSize * 2);
let borderColor = '#999999';
let childBorder = '#CCCCCC';
let subTextColor = '#999999';
let tappableColor = '#25BCCA';
let backgroundColor = '#1b9bda';
let titleColor = '#ffffff';
let nameColor = '#ffffff';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const styles = StyleSheet.create({
  textContainer: {
    flex: 1,
  },
  fullScreenContainer: {
    height: height,
    width: width,
    marginTop:20
  },
  navigationBar: {
    height: 40,
    width: width,
    paddingLeft: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#f7f03f'
  },
  backText: {
    color: '#000000',
    fontWeight: 'bold'
  },
  fullTextArea: {
    flex:3,
    height: 40,
    padding:10,
    fontSize: 10,
    textAlign: 'right',
    marginLeft: 10,
    color: titleColor,
    backgroundColor:backgroundColor,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  webView: {
    marginTop: 0,
    height: height-30,
  },
  card: {
    paddingTop: marginSize * 2,
    paddingLeft: textInset,
    paddingBottom: marginSize * 2,
    paddingRight: textInset,
    backgroundColor: backgroundColor,
    margin: marginSpace,
    borderRadius: 5,
  },
  nameText: {
    color: nameColor,
    fontWeight: "bold"
  },
  titleText: {
    color: titleColor
  }
});

module.exports = FullView;
